import 'package:coolmovies/app/modules/home/domain/usecases/list_all_movies/entity.dart';
import 'package:coolmovies/app/modules/home/domain/usecases/list_all_movies/exceptions.dart';
import 'package:coolmovies/app/modules/home/domain/usecases/list_all_movies/params.dart';
import 'package:coolmovies/app/modules/home/domain/usecases/list_all_movies/repository.dart';
import 'package:coolmovies/app/modules/home/domain/usecases/list_all_movies/usecase_impl.dart';
import 'package:either_dart/either.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class RepositoryMock extends Mock implements Repository {}

void main() {
  late RepositoryMock repositoryMock;
  late UsecaseImpl usecase;

  setUp(() {
    repositoryMock = RepositoryMock();
    usecase = UsecaseImpl(repositoryMock);
    registerFallbackValue(const Params());
  });

  test('If movies are returned correctly, respond accordingly', () {
    when(() => repositoryMock(any())).thenAnswer((invocation) async => Right(Entity.empty()));
    final futureResult = usecase(const Params());
    expect(futureResult.fold((exception) => exception, (result) => result), completion(isA<Entity>()));
  });
  test('If an exception is returned, respond accordingly', () {
    when(() => repositoryMock(any())).thenAnswer((invocation) async => const Left(UnknownException(message: 'Unable to list movies')));
    final futureResult = usecase(const Params());
    expect(futureResult.fold((exception) => exception, (result) => result), completion(isA<UnknownException>()));
  });
}
