import 'package:flutter/material.dart';

import '../../core/widgets/movie_card.dart';
import '../home/domain/usecases/list_all_movies/entity.dart';

class DesignSystemPage extends StatefulWidget {
  const DesignSystemPage({super.key});

  @override
  State<DesignSystemPage> createState() => _DesignSystemPageState();
}

class _DesignSystemPageState extends State<DesignSystemPage> with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      backgroundColor: Colors.white,
      body: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 25),
        children: [
          MovieCard(
            movie: const Movie(
              id: '1',
              title: 'fast & furious presents: hobbs & shaw',
              imgUrl: 'https://picsum.photos/60/80',
              directorName: 'David something',
            ),
            onTap: () {},
          )
        ],
      ),
    );
  }
}

class WidgetPresentation extends StatelessWidget {
  final String name;
  final Widget? child;
  final List<Widget>? children;
  final Color? backgroundColor;
  const WidgetPresentation({super.key, required this.name, this.child, this.children, this.backgroundColor});

  @override
  Widget build(BuildContext context) {
    final finalChildren = <Widget>[];
    if (children != null || child != null) {
      finalChildren.add(const SizedBox(height: 10));
    }
    if (children != null) {
      finalChildren.addAll(children!);
    }
    if (child != null) {
      finalChildren.addAll([
        child!
      ]);
    }
    final primaryColor = Theme.of(context).colorScheme.primary;
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border.all(color: primaryColor),
        color: backgroundColor,
      ),
      margin: const EdgeInsets.symmetric(vertical: 20),
      padding: const EdgeInsets.all(10),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            name,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: primaryColor),
          ),
          ...finalChildren,
        ],
      ),
    );
  }
}

class PageWidgetPresentation extends WidgetPresentation {
  const PageWidgetPresentation({super.key, required super.name, required Widget super.child});

  @override
  Widget build(BuildContext context) {
    final primaryColor = Theme.of(context).colorScheme.primary;
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border.all(color: primaryColor),
        color: backgroundColor,
      ),
      margin: const EdgeInsets.symmetric(vertical: 20),
      padding: const EdgeInsets.all(10),
      child: TextButton(
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => child!),
          );
        },
        child: Text('Next page: $name'),
      ),
    );
  }
}
