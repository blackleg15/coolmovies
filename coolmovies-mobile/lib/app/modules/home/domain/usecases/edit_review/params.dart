import '../show_movie_details/sub_entities/review.dart';

class Params {
  final Review review;

  Params({
    required this.review,
  });
}
