abstract class Exception {
  final String message;

  const Exception({required this.message});

  @override
  String toString() => message;
}

class UnknownException extends Exception {
  const UnknownException({required super.message});
}

class ExternalException extends Exception {
  const ExternalException({required super.message});
}
class MapperException extends Exception {
  const MapperException({required super.message});
}
