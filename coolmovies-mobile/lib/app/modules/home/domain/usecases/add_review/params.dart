class Params {
  final String movieId;
  final String title;
  final String userReviewerId;
  final String body;
  final int rating;

  Params({
    required this.movieId,
    required this.title,
    required this.userReviewerId,
    required this.body,
    required this.rating,
  });
}
