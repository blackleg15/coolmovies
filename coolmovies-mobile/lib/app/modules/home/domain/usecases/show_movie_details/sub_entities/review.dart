import 'reviewer.dart';
import 'stars.dart';

class Review {
  final String id;
  final String title;
  final String body;
  final Stars stars;
  final Reviewer reviewer;

  const Review({
    required this.id,
    required this.title,
    required this.body,
    required this.stars,
    required this.reviewer,
  });

  Review copyWith({String? id, String? title, String? body, Stars? stars, Reviewer? reviewer}) {
    return Review(id: id ?? this.id, title: title ?? this.title, body: body ?? this.body, stars: stars ?? this.stars, reviewer: reviewer ?? this.reviewer);
  }
}
