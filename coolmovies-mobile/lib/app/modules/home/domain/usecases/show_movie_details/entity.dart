import 'sub_entities/movie_details.dart';

class Entity {
  final MovieDetails movieDetails;

  factory Entity.empty() => Entity(movieDetails: MovieDetails.empty());

  const Entity({
    required this.movieDetails,
  });

  Entity copyWith({
    MovieDetails? movieDetails,
  }) {
    return Entity(
      movieDetails: movieDetails ?? this.movieDetails,
    );
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Entity && other.movieDetails == movieDetails;
  }

  @override
  int get hashCode => movieDetails.hashCode;
}
