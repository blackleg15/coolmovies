import 'package:collection/collection.dart';

import 'review.dart';

class MovieDetails {
  final String id;
  final String title;
  final String imgUrl;
  final List<Review> reviews;
  final String directorName;
  final String releaseDate;

  const MovieDetails({
    required this.id,
    required this.title,
    required this.imgUrl,
    required this.reviews,
    required this.directorName,
    required this.releaseDate,
  });

  factory MovieDetails.empty() => const MovieDetails(id: '', title: '', imgUrl: '', reviews: [], directorName: '', releaseDate: '');

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    final listEquals = const DeepCollectionEquality().equals;

    return other is MovieDetails && other.id == id && other.title == title && other.imgUrl == imgUrl && listEquals(other.reviews, reviews) && other.directorName == directorName && other.releaseDate == releaseDate;
  }

  @override
  int get hashCode {
    return id.hashCode ^ title.hashCode ^ imgUrl.hashCode ^ reviews.hashCode ^ directorName.hashCode ^ releaseDate.hashCode;
  }

  MovieDetails copyWith({
    String? id,
    String? title,
    String? imgUrl,
    List<Review>? reviews,
    String? directorName,
    String? releaseDate,
  }) {
    return MovieDetails(
      id: id ?? this.id,
      title: title ?? this.title,
      imgUrl: imgUrl ?? this.imgUrl,
      reviews: reviews ?? this.reviews,
      directorName: directorName ?? this.directorName,
      releaseDate: releaseDate ?? this.releaseDate,
    );
  }
}
