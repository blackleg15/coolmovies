class Reviewer {
  final String id;
  final String name;

  const Reviewer({
    required this.id,
    required this.name,
  });
}
