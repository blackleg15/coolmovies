import 'entity.dart';
import 'exceptions.dart';
import 'package:either_dart/either.dart';

typedef Result = Future<Either<Exception, Entity>>;
