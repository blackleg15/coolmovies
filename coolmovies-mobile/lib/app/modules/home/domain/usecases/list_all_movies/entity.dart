import 'package:collection/collection.dart';

class Entity {
  final List<Movie> movies;

  factory Entity.empty() => const Entity(movies: []);

  const Entity({
    required this.movies,
  });

  Entity copyWith({
    List<Movie>? movies,
  }) {
    return Entity(
      movies: movies ?? this.movies,
    );
  }

  @override
  String toString() => 'Entity(movies: $movies)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    final listEquals = const DeepCollectionEquality().equals;

    return other is Entity && listEquals(other.movies, movies);
  }

  @override
  int get hashCode => movies.hashCode;
}

class Movie {
  final String id;
  final String title;
  final String imgUrl;
  final String directorName;

  const Movie({
    required this.id,
    required this.title,
    required this.imgUrl,
    required this.directorName
  });


  Movie copyWith({
    String? id,
    String? title,
    String? imgUrl,
    String? directorName,
  }) {
    return Movie(
      id: id ?? this.id,
      title: title ?? this.title,
      imgUrl: imgUrl ?? this.imgUrl,
      directorName: directorName ?? this.directorName,
    );
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
  
    return other is Movie && other.id == id && other.title == title && other.imgUrl == imgUrl && other.directorName == directorName;
  }

  @override
  int get hashCode {
    return id.hashCode ^ title.hashCode ^ imgUrl.hashCode ^ directorName.hashCode;
  }
}
