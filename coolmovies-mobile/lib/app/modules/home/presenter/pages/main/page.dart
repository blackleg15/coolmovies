import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'blocs/usecases/list_all_movies/bloc.dart';
import 'controller.dart';
import '../../../../../core/widgets/movie_card.dart';

class Page extends StatefulWidget {
  const Page({super.key});

  @override
  State<Page> createState() => _PageState();
}

class _PageState extends State<Page> {
  final Controller controller = Modular.get();

  @override
  void initState() {
    super.initState();
    controller.getMovies();
  }

  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Movies'),
        actions: [
          ElevatedButton.icon(
              label: const Text(
                'Logout',
                //style: TextStyle(color: Colors.white),
              ),
              onPressed: () async {
                setState(() {
                  isLoading = true;
                });
                await controller.logout();
                setState(() {
                  isLoading = false;
                });
              },
              icon: const Icon(
                Icons.logout,
                //color: Colors.white,
              ))
        ],
      ),
      body: BlocBuilder<UsecaseBloc, BlocState>(
        bloc: controller.getMoviesBloc,
        builder: (context, state) {
          if (state is LoadingState) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          if (state is SuccessState) {
            return ListView.separated(
              padding: const EdgeInsets.only(top: 20, left: 10, right: 10),
              itemBuilder: (context, index) => MovieCard(
                key: Key(state.movies[index].id),
                movie: state.movies[index],
                onTap: () {
                  Modular.to.pushNamed('details/${state.movies[index].id}');
                },
              ),
              separatorBuilder: (context, index) => const SizedBox(height: 10),
              itemCount: state.movies.length,
            );
          }
          return const SizedBox();
        },
      ),
    );
  }
}
