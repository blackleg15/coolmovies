import 'package:flutter_modular/flutter_modular.dart';

import '../../../../../../domain/usecases/list_all_movies/usecase_impl.dart';
import '../../../../../../external/usecases/list_all_movies/graphql/mapper.dart';
import '../../../../../../external/usecases/list_all_movies/graphql/repository_impl.dart';
import 'bloc.dart';

final usecaseBinds = <Bind>[
  Bind.singleton<UsecaseBloc>((i) => UsecaseBloc(i()), onDispose: (value) => value.close()),
  Bind.singleton((i) => UsecaseImpl(i())),
  Bind.singleton((i) => RepositoryImpl(i(), i())),
  Bind.singleton((i) => const Mapper()),
  //Bind.singleton((i) => RepositoryImpl(i())),
  //Bind.singleton((i) => const Mapper()),
];
