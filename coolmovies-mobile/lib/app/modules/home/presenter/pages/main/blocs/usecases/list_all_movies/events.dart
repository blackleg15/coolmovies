part of 'bloc.dart';

abstract class BlocEvent {
  const BlocEvent();
}

class GetMoviesEvent extends BlocEvent {
  final Params params;

  const GetMoviesEvent(this.params);
}
