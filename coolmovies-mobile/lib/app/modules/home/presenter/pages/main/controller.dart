import 'package:flutter_modular/flutter_modular.dart';

import '../../../../../core/session/session_store.dart';
import '../../../domain/usecases/list_all_movies/params.dart';
import 'blocs/usecases/list_all_movies/bloc.dart' as get_movies;

class Controller {
  final SessionStore _sessionStore;
  const Controller(this.getMoviesBloc, this._sessionStore);

  final get_movies.UsecaseBloc getMoviesBloc;

  void getMovies() {
    getMoviesBloc.add(const get_movies.GetMoviesEvent(Params()));
  }

  Future<void> logout() => _sessionStore.logout().then((value) => Modular.to.pushNamedAndRemoveUntil('../auth/', (_) => false));
}
