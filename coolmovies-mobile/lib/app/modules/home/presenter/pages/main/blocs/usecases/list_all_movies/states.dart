part of 'bloc.dart';

class BlocState {
  final List<Movie> movies;

  const BlocState({required this.movies});
}

class InitialState extends BlocState {
  const InitialState() : super(movies: const []);
}

class LoadingState extends BlocState {
  const LoadingState(List<Movie> movies) : super(movies: movies);
}

class SuccessState extends BlocState {
  const SuccessState(List<Movie> movies) : super(movies: movies);
}

class ExceptionState extends BlocState {
  final Exception exception;

  const ExceptionState(List<Movie> movies, this.exception) : super(movies: movies);
}
