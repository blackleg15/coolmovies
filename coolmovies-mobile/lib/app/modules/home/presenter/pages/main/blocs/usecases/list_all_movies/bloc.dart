import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../../domain/usecases/list_all_movies/entity.dart';
import '../../../../../../domain/usecases/list_all_movies/params.dart';
import '../../../../../../domain/usecases/list_all_movies/usecase.dart';
import '../../../../../../domain/usecases/list_all_movies/exceptions.dart';

part 'events.dart';
part 'states.dart';

class UsecaseBloc extends Bloc<BlocEvent, BlocState> {
  final Usecase getMovies;

  UsecaseBloc(this.getMovies) : super(const InitialState()) {
    on<GetMoviesEvent>((event, emit) async {
      emit(LoadingState(state.movies));
      final result = await getMovies(event.params);
      final newState = result.fold(
        (exception) => ExceptionState(state.movies, exception),
        (entity) => SuccessState(entity.movies),
      );
      emit(newState);
    });
  }
}
