part of 'bloc.dart';

class BlocState {
  const BlocState();
}

class InitialState extends BlocState {
  InitialState();
}

class LoadingState extends BlocState {
  const LoadingState();
}

class SuccessState extends BlocState {
  const SuccessState();
}

class ExceptionState extends BlocState {
  final Exception exception;

  const ExceptionState({required this.exception});
}
