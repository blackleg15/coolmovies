part of 'bloc.dart';

abstract class BlocEvent {
  const BlocEvent();
}

class StartEditingReview extends BlocEvent {
  final Review review;

  const StartEditingReview(this.review);
}

class Cancel extends BlocEvent {}

class SendEditedReview extends BlocEvent {
  final Review editedReview;

  SendEditedReview(this.editedReview);
}
