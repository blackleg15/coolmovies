import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../domain/usecases/show_movie_details/sub_entities/review.dart';
import '../../../domain/usecases/show_movie_details/params.dart';
import '../../../domain/usecases/show_movie_details/sub_entities/movie_details.dart';
import 'blocs/usecases/show_movie_details/bloc.dart';
import 'controller.dart';
import 'widgets/add_review_widget.dart';
import 'widgets/edit_review_widget.dart';
import 'widgets/rating_widget.dart';

class Page extends StatefulWidget {
  final String movieId;
  const Page({super.key, required this.movieId});

  @override
  State<Page> createState() => _PageState();
}

class _PageState extends State<Page> {
  final Controller controller = Modular.get();
  late String movieId;

  @override
  void initState() {
    movieId = widget.movieId;
    getMovieDetails(movieId);
    super.initState();
  }

  // @override
  // void didUpdateWidget(covariant Page oldWidget) {
  //   if (oldWidget.movieId != widget.movieId) {
  //     movieId = widget.movieId;
  //     getMovieDetails(movieId);
  //   }
  //   super.didUpdateWidget(oldWidget);
  // }

  void getMovieDetails(String id) => controller.showMovieDetailsById(Params(id));

  double getAverageRating(MovieDetails movieDetails) {
    var ratingSum = 0.0;
    for (final review in movieDetails.reviews) {
      final rating = review.stars.index + 1;
      ratingSum += rating;
    }
    return ratingSum / movieDetails.reviews.length;
  }

  void onSuccessfulReviewAdding() => getMovieDetails(movieId);

  void onSuccessfulReviewEditing(Review review) => controller.notifyReviewChange(review);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: Container(
          margin: const EdgeInsets.only(left: 20),
          child: BackButton(color: Theme.of(context).textTheme.bodyLarge?.color),
        ),
        automaticallyImplyLeading: false,
        backgroundColor: const Color(0xFF212121),
      ),
      body: BlocBuilder<UsecaseBloc, BlocState>(
        bloc: controller.showMoviewDetailsBloc,
        builder: (context, state) {
          if (state is SuccessState) {
            final reviews = state.movieDetails.reviews;
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 60,
                            width: 60,
                            child: Hero(
                              tag: state.movieDetails.id,
                              child: CircleAvatar(
                                foregroundImage: NetworkImage(state.movieDetails.imgUrl),
                              ),
                            ),
                          ),
                          const SizedBox(width: 20),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  state.movieDetails.title,
                                  textAlign: TextAlign.left,
                                  style: const TextStyle(
                                    color: Colors.white,
                                    fontSize: 22,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                const SizedBox(height: 5),
                                Text(
                                  'by ${state.movieDetails.directorName}',
                                  style: const TextStyle(color: Colors.white),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      //RELEASE DATE & RATING
                      const SizedBox(
                        height: 40,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                'Release Date',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Text(
                                state.movieDetails.releaseDate,
                                style: const TextStyle(color: Colors.white),
                              ),
                            ],
                          ),
                          RatingWidget(rating: getAverageRating(state.movieDetails).toStringAsFixed(1)),
                        ],
                      ),
                      const SizedBox(height: 20),
                      //REVIEWS
                      const Text(
                        'Reviews',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: ListView.separated(
                    separatorBuilder: (context, index) => const Divider(height: 24),
                    itemCount: reviews.length + 2,
                    itemBuilder: (context, index) {
                      final isTheFirstElementOfTheList = index == 0;
                      if (isTheFirstElementOfTheList) {
                        return Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 30),
                          child: AddReviewWidget(
                              movieId: movieId,
                              addReviewBloc: controller.addReviewBloc,
                              userId: controller.userId,
                              onSuccess: onSuccessfulReviewAdding),
                        );
                      }
                      final isTheLastElementOfTheList = index == reviews.length + 1;
                      if (isTheLastElementOfTheList) {
                        return const SizedBox(height: 20);
                      }
                      final actualReviewListIndex = index - 1;
                      final review = reviews[actualReviewListIndex];
                      return Container(
                        margin: const EdgeInsets.symmetric(horizontal: 14),
                        child: EditReviewWidget(
                            key: Key(review.id),
                            review: review,
                            userId: controller.userId,
                            editReviewBloc: controller.editReviewBloc,
                            onSuccess: onSuccessfulReviewEditing),
                      );
                    },
                  ),
                ),
              ],
            );
          }
          if (state is ExceptionState) {
            return Center(
              child: Text(state.exception.toString()),
            );
          }
          if (state is LoadingState) {
            return const Center(
              child: CircularProgressIndicator.adaptive(),
            );
          }
          return const SizedBox();
        },
      ),
    );
  }
}
