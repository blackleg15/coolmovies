part of 'bloc.dart';

abstract class BlocEvent {
  const BlocEvent();
}

class ShowMovieDetails extends BlocEvent {
  final Params params;

  const ShowMovieDetails(this.params);
}

class ReviewChange extends BlocEvent {
  final Review review;
  const ReviewChange({required this.review});
}
