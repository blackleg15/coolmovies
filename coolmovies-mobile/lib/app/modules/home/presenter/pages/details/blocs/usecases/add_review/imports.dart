import 'package:flutter_modular/flutter_modular.dart';

import '../../../../../../domain/usecases/add_review/usecase_impl.dart';
import '../../../../../../external/usecases/add_review/graphql/mapper.dart';
import '../../../../../../external/usecases/add_review/graphql/repository_impl.dart';
//import '../../../../../../external/usecases/show_movie_details/mock/mapper.dart';
//import '../../../../../../external/usecases/show_movie_details/mock/repository_impl.dart';
import 'bloc.dart';

final usecaseBinds = <Bind>[
  Bind.singleton<UsecaseBloc>((i) => UsecaseBloc(i()), onDispose: (bloc) => bloc.close()),
  Bind.singleton((i) => UsecaseImpl(i())),
  Bind.singleton((i) => RepositoryImpl(i(), i())),
  Bind.singleton((i) => const Mapper()),
  //Bind.singleton((i) => RepositoryImpl(i())),
  //Bind.singleton((i) => const Mapper()),
];
