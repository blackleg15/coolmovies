import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'stars_popup_menu_button.dart';
import '../../../../domain/usecases/add_review/params.dart';
import '../blocs/usecases/add_review/bloc.dart';

class AddReviewWidget extends StatefulWidget {
  final UsecaseBloc addReviewBloc;
  final VoidCallback? onSuccess;
  final String movieId;
  final String userId;
  const AddReviewWidget({super.key, required this.addReviewBloc, this.onSuccess, required this.movieId, required this.userId});

  @override
  State<AddReviewWidget> createState() => _AddReviewWidgetState();
}

class _AddReviewWidgetState extends State<AddReviewWidget> {
  late UsecaseBloc addReviewBloc;

  String newReviewTitle = '';
  String newReviewBody = '';
  String newReviewRating = '';

  bool canSendNewReview = false;

  void validateNewReviewFields() {
    canSendNewReview = newReviewTitle.isNotEmpty && newReviewBody.isNotEmpty && newReviewRating.isNotEmpty;
  }

  void resetFieldsAfterSendReview() {
    newReviewTitle = '';
    newReviewBody = '';
    newReviewRating = '';
    canSendNewReview = false;
  }

  @override
  void initState() {
    super.initState();
    addReviewBloc = widget.addReviewBloc;
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<UsecaseBloc, BlocState>(
      bloc: widget.addReviewBloc,
      listener: (context, state) {
        if (state is SuccessState) {
          widget.onSuccess?.call();
        }
      },
      builder: (context, state) {
        if (state is LoadingState) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        if (state is ExceptionState) {
          return Text(state.toString());
        }
        return Column(
          children: [
            const SizedBox(height: 20),
            const Text(
              'Send your own review',
              style: TextStyle(
                color: Colors.white,
                fontSize: 15,
                fontWeight: FontWeight.bold,
              ),
            ),
            TextField(
              decoration: const InputDecoration(
                label: Text('Title'),
                floatingLabelStyle: TextStyle(
                  color: Colors.white,
                ),
                labelStyle: TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                ),
              ),
              onChanged: (value) {
                newReviewTitle = value;
                validateNewReviewFields();
              },
            ),
            TextField(
              decoration: InputDecoration(
                label: const Text('Review'),
                hintText: 'What does this movie make you feel...?',
                hintStyle: TextStyle(color: Colors.white.withOpacity(0.2), fontSize: 13),
                floatingLabelStyle: const TextStyle(
                  color: Colors.white,
                ),
                labelStyle: const TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                ),
              ),
              style: const TextStyle(color: Colors.white),
              onChanged: (value) {
                newReviewBody = value;
                validateNewReviewFields();
              },
            ),
            Row(
              children: [
                StarsPopupMenuButton(
                  label: newReviewRating,
                  onSelected: (value) {
                    setState(() {
                      newReviewRating = (value.index + 1).toString();
                      validateNewReviewFields();
                    });
                  },
                ),
                const Spacer(),
                ElevatedButton(
                  onPressed: !canSendNewReview
                      ? null
                      : () {
                          if (!canSendNewReview) return;
                          final title = newReviewTitle;
                          final body = newReviewBody;
                          final rating = newReviewRating;
                          addReviewBloc.add(
                            AddReview(Params(
                              movieId: widget.movieId,
                              title: title,
                              userReviewerId: widget.userId,
                              body: body,
                              rating: int.parse(rating),
                            )),
                          );
                        },
                  style: ElevatedButton.styleFrom(foregroundColor: canSendNewReview ? Colors.blue : Theme.of(context).disabledColor),
                  child: Text(
                    'Send review',
                    style: TextStyle(color: canSendNewReview ? const Color(0xFF212121) : Colors.white.withOpacity(0.2)),
                  ),
                ),
              ],
            ),
          ],
        );
      },
    );
  }
}
