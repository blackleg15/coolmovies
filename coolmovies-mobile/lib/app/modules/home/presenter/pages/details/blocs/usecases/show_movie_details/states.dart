part of 'bloc.dart';

class BlocState {
  final MovieDetails movieDetails;

  const BlocState({required this.movieDetails});
}

class InitialState extends BlocState {
  InitialState() : super(movieDetails: MovieDetails.empty());
}

class LoadingState extends BlocState {
  const LoadingState(MovieDetails movieDetails) : super(movieDetails: movieDetails);
}

class SuccessState extends BlocState {
  const SuccessState(MovieDetails movieDetails) : super(movieDetails: movieDetails);
}

class ExceptionState extends BlocState {
  final Exception exception;

  const ExceptionState(MovieDetails movieDetails, this.exception) : super(movieDetails: movieDetails);
}
