import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../../domain/usecases/show_movie_details/params.dart';
import '../../../../../../domain/usecases/show_movie_details/sub_entities/movie_details.dart';
import '../../../../../../domain/usecases/show_movie_details/sub_entities/review.dart';
import '../../../../../../domain/usecases/show_movie_details/usecase.dart';
import '../../../../../../domain/usecases/show_movie_details/exceptions.dart';

part 'events.dart';
part 'states.dart';

class UsecaseBloc extends Bloc<BlocEvent, BlocState> {
  final Usecase showMoviewDetails;

  UsecaseBloc(this.showMoviewDetails) : super(InitialState()) {
    on<ShowMovieDetails>((event, emit) async {
      emit(LoadingState(state.movieDetails));
      final result = await showMoviewDetails(event.params);
      final newState = result.fold(
        (exception) => ExceptionState(state.movieDetails, exception),
        (entity) => SuccessState(entity.movieDetails),
      );
      emit(newState);
    });
    on<ReviewChange>((event, emit) {
      if (state is SuccessState) {
        final editedReview = event.review;
        final reviewId = editedReview.id;
        final oldReviewsList = state.movieDetails.reviews;
        final changedReviewIndex = oldReviewsList.indexWhere((review) => review.id == reviewId);
        final newReviewsList = [
          ...state.movieDetails.reviews
        ];
        newReviewsList[changedReviewIndex] = editedReview;
        final newMovieDetails = state.movieDetails.copyWith(reviews: newReviewsList);
        emit(SuccessState(newMovieDetails));
      }
    });
  }
}
