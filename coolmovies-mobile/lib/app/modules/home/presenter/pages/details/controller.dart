import '../../../../../core/session/session_store.dart';
import '../../../domain/usecases/show_movie_details/params.dart' as movie_details;
import '../../../domain/usecases/add_review/params.dart' as review;
import '../../../domain/usecases/show_movie_details/sub_entities/review.dart';
import 'blocs/usecases/show_movie_details/bloc.dart' as show_movie_details;
import 'blocs/usecases/add_review/bloc.dart' as add_review;
import 'blocs/usecases/edit_review/bloc.dart' as edit_review;

class Controller {
  final show_movie_details.UsecaseBloc showMoviewDetailsBloc;
  final add_review.UsecaseBloc addReviewBloc;
  final edit_review.UsecaseBloc editReviewBloc;
  final SessionStore _sessionStore;

  const Controller(this.showMoviewDetailsBloc, this.addReviewBloc, this._sessionStore, this.editReviewBloc);

  void showMovieDetailsById(movie_details.Params params) {
    showMoviewDetailsBloc.add(show_movie_details.ShowMovieDetails(params));
  }

  void addReview(review.Params params) {
    addReviewBloc.add(add_review.AddReview(params));
  }

  String get userId => _sessionStore.loggedUser?.id ?? '';

  void notifyReviewChange(Review review) {
    showMoviewDetailsBloc.add(show_movie_details.ReviewChange(review: review));
  }
}
