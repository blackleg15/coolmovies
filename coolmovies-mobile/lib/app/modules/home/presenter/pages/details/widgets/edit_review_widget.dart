import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'stars_popup_menu_button.dart';
import '../../../../domain/usecases/show_movie_details/sub_entities/review.dart';
import '../../../../domain/usecases/show_movie_details/sub_entities/stars.dart';
import '../blocs/usecases/edit_review/bloc.dart';

class EditReviewWidget extends StatefulWidget {
  final UsecaseBloc editReviewBloc;
  final void Function(Review) onSuccess;
  final Review review;
  final String userId;
  const EditReviewWidget({super.key, required this.onSuccess, required this.review, required this.userId, required this.editReviewBloc});

  @override
  State<EditReviewWidget> createState() => _EditReviewWidgetState();
}

class _EditReviewWidgetState extends State<EditReviewWidget> {
  late UsecaseBloc editReviewBloc;
  late Review review;

  int newEditedRating = 0;
  late TextEditingController titleController;
  late TextEditingController bodyController;

  @override
  void initState() {
    editReviewBloc = widget.editReviewBloc;
    review = widget.review;
    super.initState();
  }

  @override
  void didUpdateWidget(covariant EditReviewWidget oldWidget) {
    if (oldWidget.review.title != widget.review.title || oldWidget.review.body != widget.review.body || oldWidget.review.stars != widget.review.stars) {
      review = widget.review;
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<UsecaseBloc, BlocState>(
      listener: (context, state) {
        if (state is Success && state.editedReview.id == review.id) {
          widget.onSuccess(state.editedReview);
        }
      },
      bloc: editReviewBloc,
      builder: (context, state) {
        if (state is Editing && state.review.id == review.id) {
          return Container(
            margin: const EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const Text(
                  'Edit your review',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                TextField(
                  controller: titleController,
                ),
                TextField(
                  controller: bodyController,
                  maxLines: 5,
                ),
                StatefulBuilder(
                  builder: (context, setState) => StarsPopupMenuButton(
                    label: newEditedRating.toString(),
                    initialValue: Stars.values.elementAt(newEditedRating - 1),
                    onSelected: (value) {
                      setState(() {
                        newEditedRating = value.index + 1;
                      });
                    },
                  ),
                ),
                ButtonBar(
                  children: [
                    ElevatedButton(
                        onPressed: () {
                          editReviewBloc.add(Cancel());
                        },
                        child: const Text('Cancel')),
                    ElevatedButton(
                        onPressed: () {
                          editReviewBloc.add(SendEditedReview(Review(id: review.id, title: titleController.text, body: bodyController.text, stars: Stars.values.elementAt(newEditedRating - 1), reviewer: review.reviewer)));
                        },
                        child: const Text('Edit')),
                  ],
                ),
              ],
            ),
          );
        } else {
          return Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const CircleAvatar(child: Icon(Icons.account_circle)),
              Expanded(
                child: ListTile(
                  title: Text(
                    review.title,
                    style: const TextStyle(color: Colors.white, fontWeight: FontWeight.w700, fontSize: 16),
                  ),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        review.body,
                        textAlign: TextAlign.justify,
                        style: const TextStyle(color: Colors.white),
                      ),
                      const SizedBox(height: 5),
                      Text(
                        "Author: ${review.reviewer.name}",
                        textAlign: TextAlign.justify,
                        style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15),
                      ),
                    ],
                  ),
                  trailing: review.reviewer.id == widget.userId && state is! Editing
                      ? IconButton(
                          icon: const Icon(
                            Icons.edit,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            titleController = TextEditingController(text: review.title);
                            bodyController = TextEditingController(text: review.body);
                            newEditedRating = review.stars.index + 1;
                            editReviewBloc.add(StartEditingReview(review));
                          },
                        )
                      : null,
                ),
              ),
            ],
          );
        }
      },
    );
  }
}
