part of 'bloc.dart';

abstract class BlocState {
  const BlocState();
}

class NotEditing extends BlocState {}

class Editing extends BlocState {
  final Review review;

  const Editing(this.review);
}

class Loading extends BlocState {}

class Exception extends BlocState {}

class Success extends BlocState {
  final Review editedReview;

  Success(this.editedReview);
}
