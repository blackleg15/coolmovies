import 'package:flutter/material.dart';

import '../../../../domain/usecases/show_movie_details/sub_entities/stars.dart';

class StarsPopupMenuButton extends StatelessWidget {
  final String label;
  final void Function(Stars) onSelected;
  final Stars? initialValue;
  const StarsPopupMenuButton({super.key, required this.label, required this.onSelected, this.initialValue});

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<Stars>(
      initialValue: initialValue,
      onSelected: onSelected,
      color: const Color(0xFF3D3D3D),
      itemBuilder: (BuildContext context) => List.generate(
        Stars.values.length,
        (i) => PopupMenuItem<Stars>(
          value: Stars.values.firstWhere((e) => e.value == i),
          child: Text('$i'),
        ),
      ),
      child: Row(
        children: [
          const Text('How many stars?'),
          const SizedBox(
            width: 10,
          ),
          Text(label),
          const SizedBox(
            width: 4,
          ),
          Icon(
            Icons.star,
            color: label.isNotEmpty ? Colors.amber : const Color(0xFF3D3D3D),
          ),
        ],
      ),
    );
  }
}
