import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../../domain/usecases/edit_review/params.dart';
import '../../../../../../domain/usecases/edit_review/usecase.dart';
import '../../../../../../domain/usecases/show_movie_details/sub_entities/review.dart';

part 'events.dart';
part 'states.dart';

class UsecaseBloc extends Bloc<BlocEvent, BlocState> {
  final Usecase usecase;

  UsecaseBloc(this.usecase) : super(NotEditing()) {
    on<StartEditingReview>((event, emit) {
      if (state is StartEditingReview) {
        return;
      }
      emit(Editing(event.review));
    });
    on<Cancel>((event, emit) {
      emit(NotEditing());
    });
    on<SendEditedReview>((event, emit) async {
      emit(Loading());
      final result = await usecase(Params(review: event.editedReview));
      result.fold((left) {
        emit(Exception());
      }, (right) {
        emit(Success(event.editedReview));
        emit(NotEditing());
      });
    });
  }
}
