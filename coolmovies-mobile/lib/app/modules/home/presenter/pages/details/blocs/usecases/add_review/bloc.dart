import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../../domain/usecases/add_review/params.dart';
import '../../../../../../domain/usecases/add_review/usecase.dart';
import '../../../../../../domain/usecases/add_review/exceptions.dart';

part 'events.dart';
part 'states.dart';

class UsecaseBloc extends Bloc<BlocEvent, BlocState> {
  final Usecase showMoviewDetails;

  UsecaseBloc(this.showMoviewDetails) : super(InitialState()) {
    on<AddReview>((event, emit) async {
      emit(const LoadingState());
      final result = await showMoviewDetails(event.params);
      final newState = result.fold(
        (exception) => ExceptionState(exception: exception),
        (entity) => const SuccessState(),
      );
      emit(newState);
    });
  }
}
