import 'package:flutter_modular/flutter_modular.dart';

import '../../../../../../domain/usecases/edit_review/usecase_impl.dart';
import '../../../../../../external/usecases/edit_review/graphql/mapper.dart';
import '../../../../../../external/usecases/edit_review/graphql/repository_impl.dart';
import 'bloc.dart';

final usecaseBinds = <Bind>[
  Bind.singleton<UsecaseBloc>((i) => UsecaseBloc(i()), onDispose: (bloc) => bloc.close()),
  Bind.singleton((i) => UsecaseImpl(i())),
  Bind.singleton((i) => RepositoryImpl(i(), i())),
  Bind.singleton((i) => const Mapper()),
  //Bind.singleton((i) => RepositoryImpl(i())),
  //Bind.singleton((i) => const Mapper()),
];
