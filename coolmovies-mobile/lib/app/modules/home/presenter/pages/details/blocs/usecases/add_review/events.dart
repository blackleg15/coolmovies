part of 'bloc.dart';

abstract class BlocEvent {
  const BlocEvent();
}

class AddReview extends BlocEvent {
  final Params params;

  const AddReview(this.params);
}
