import 'package:flutter_modular/flutter_modular.dart';

import 'pages/details/blocs/usecases/edit_review/imports.dart' as edit_review;
import 'pages/details/blocs/usecases/show_movie_details/imports.dart' as details;
import 'pages/details/blocs/usecases/add_review/imports.dart' as add_review;
import 'pages/details/controller.dart' as details;
import 'pages/main/blocs/usecases/list_all_movies/imports.dart' as list;
import 'pages/main/controller.dart' as main;
import 'pages/main/page.dart' as main;
import 'pages/details/page.dart' as details;

class HomeModule extends Module {
  @override
  final List<Bind<Object>> binds = [
    ...list.usecaseBinds,
    ...details.usecaseBinds,
    ...add_review.usecaseBinds,
    ...edit_review.usecaseBinds,
    Bind.singleton((i) => main.Controller(i(), i())),
    Bind.singleton((i) => details.Controller(i(), i(), i(), i())),
  ];

  @override
  final List<ModularRoute> routes = [
    ChildRoute('/', child: (_, __) => const main.Page()),
    ChildRoute('/details/:id', child: (_, args) => details.Page(movieId: args.params['id'] ?? ''))
  ];
}
