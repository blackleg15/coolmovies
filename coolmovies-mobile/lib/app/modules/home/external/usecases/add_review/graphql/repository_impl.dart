import 'package:either_dart/either.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import '../../../../domain/usecases/add_review/params.dart';
import '../../../../domain/usecases/add_review/exceptions.dart' as exceptions;
import '../../../../domain/usecases/add_review/repository.dart';
import '../../../../domain/usecases/add_review/result_typedef.dart';
import 'mapper.dart';

class RepositoryImpl implements Repository {
  final GraphQLClient client;
  final Mapper _mapper;

  RepositoryImpl(this.client, this._mapper);

  @override
  Result call(Params params) async {
    try {
      final QueryResult result = await client.mutate(
        MutationOptions(
          document: gql(_mutation),
          variables: {
            "movieId": params.movieId,
            "title": params.title,
            "userReviewerId": params.userReviewerId,
            "body": params.body,
            "rating": params.rating
          },
        ),
      );

      if (result.hasException) {
        return const Left(exceptions.ExternalException(message: 'Unable to establish a connection'));
      }

      final resultData = result.data;

      try {
        if (resultData != null) {
          final formattedResult = _mapper.fromResultToEntity(resultData);
          return Right(formattedResult);
        }
      } catch (e) {
        return const Left(exceptions.UnknownException(message: 'Unable to add a review'));
      }
      return const Left(exceptions.UnknownException(message: 'Unable to add a review'));
    } catch (e) {
      return const Left(exceptions.UnknownException(message: 'Unexpected result: Review result'));
    }
  }

  final _mutation = r"""
          mutation AddReview($movieId: UUID!, $title: String!, $userReviewerId: UUID!, $rating: Int!, $body: String!) {
            createMovieReview(
              input: {movieReview: {title: $title, movieId: $movieId, userReviewerId: $userReviewerId, rating: $rating, body: $body}}
            ) {
              clientMutationId
            }
          }
        """;
}
