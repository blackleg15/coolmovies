import '../../../../domain/usecases/list_all_movies/entity.dart';
import '../../../../domain/usecases/list_all_movies/exceptions.dart';

class Mapper {
  Entity fromResultToEntity(Map<String, dynamic> result) {
    if (!result.containsKey('allMovies')) {
      throw const MapperException(message: 'Unexpected result format: allMovies');
    }
    final allMovies = result['allMovies'];
    if (allMovies is! Map) {
      throw const MapperException(message: 'Unexpected result type: allMovies');
    }

    if (!allMovies.containsKey('nodes')) {
      throw const MapperException(message: 'Unexpected result format: nodes');
    }
    final nodes = allMovies['nodes'];
    if (nodes is! List) {
      throw const MapperException(message: 'Unexpected result type: nodes');
    }

    final movies = <Movie>[];
    for (final movieData in nodes) {
      if (movieData is! Map) {
        throw const MapperException(message: 'Unexpected type: movie node');
      }

      if (!movieData.containsKey('id')) {
        throw const MapperException(message: "Movie's id is missing");
      }

      final id = movieData['id'];

      if (id is! String) {
        throw const MapperException(message: "Unexpected type: Movie's id");
      }

      if (!movieData.containsKey('title')) {
        throw const MapperException(message: "Movie's title is missing");
      }

      final title = movieData['title'];

      if (title is! String) {
        throw const MapperException(message: "Unexpected type: Movie's title");
      }

      if (!movieData.containsKey('imgUrl')) {
        throw const MapperException(message: "Movie's imgUrl is missing");
      }

      final imgUrl = movieData['imgUrl'];

      if (imgUrl is! String) {
        throw const MapperException(message: "Unexpected type: Movie's imgUrl");
      }

      if (!movieData.containsKey('movieDirectorByMovieDirectorId')) {
        throw const MapperException(message: "The director of the movie is missing");
      }

      final movieDirector = movieData['movieDirectorByMovieDirectorId'];

      if (movieDirector is! Map) {
        throw const MapperException(message: "Unexpected type: movie director");
      }
      if (!movieDirector.containsKey('name')) {
        throw const MapperException(message: "The movie director's name is missing");
      }

      final directorName = movieDirector['name'];

      if (directorName is! String) {
        throw const MapperException(message: "Unexpected type: movie director's name");
      }

      final movie = Movie(id: id, title: title, imgUrl: imgUrl, directorName: directorName);

      movies.add(movie);
    }

    return Entity(movies: movies);
  }

  const Mapper();
}
