import 'package:either_dart/either.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import '../../../../domain/usecases/list_all_movies/exceptions.dart' as exceptions;
import '../../../../domain/usecases/list_all_movies/params.dart';
import '../../../../domain/usecases/list_all_movies/repository.dart';
import '../../../../domain/usecases/list_all_movies/result_typedef.dart';
import 'mapper.dart';

class RepositoryImpl implements Repository {
  final GraphQLClient client;
  final Mapper _mapper;

  RepositoryImpl(this.client, this._mapper);

  @override
  Result call(Params params) async {
    try {
      final QueryResult result = await client.query(QueryOptions(
        document: gql(_query),
      ));

      if (result.hasException) {
        return const Left(exceptions.ExternalException(message: 'Unable to establish a connection'));
      }

      final resultData = result.data;

      if (resultData != null) {
        final formattedResult = _mapper.fromResultToEntity(resultData);
        return Right(formattedResult);
      }

      return const Left(exceptions.UnknownException(message: 'Unable to list movies'));
    } catch (e) {
      return const Left(exceptions.UnknownException(message: 'Unexpected result: List Movies'));
    }
  }

  final _query = r"""
          query AllMovies {
            allMovies {
              nodes {
                id
                imgUrl
                movieDirectorId
                userCreatorId
                title
                releaseDate
                nodeId
                userByUserCreatorId {
                  id
                  name
                  nodeId
                }
                movieDirectorByMovieDirectorId {
                  name
                }
              }
            }
          }
        """;
}
