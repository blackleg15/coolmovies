import '../../../../domain/usecases/list_all_movies/entity.dart';

class Mapper {
  Entity fromResultToEntity(Map<String, dynamic> result) {
    const id = 'Movie id';
    const title = 'Movie title';
    const directorName = "Movie director's name";
    const movies = <Movie>[
      Movie(id: id, title: title, imgUrl: 'https://api.lorem.space/image/movie?w=60&h=80&hash=g726ljt4', directorName: directorName),
    ];

    return const Entity(movies: movies);
  }

  const Mapper();
}
