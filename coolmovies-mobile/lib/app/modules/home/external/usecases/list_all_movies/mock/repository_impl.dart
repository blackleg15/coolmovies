import 'package:either_dart/either.dart';

import '../../../../domain/usecases/list_all_movies/params.dart';
import '../../../../domain/usecases/list_all_movies/repository.dart';
import '../../../../domain/usecases/list_all_movies/result_typedef.dart';
import 'mapper.dart';

class RepositoryImpl implements Repository {
  final Mapper _mapper;

  RepositoryImpl(this._mapper);

  @override
  Result call(Params params) async => Right(_mapper.fromResultToEntity({}));
}
