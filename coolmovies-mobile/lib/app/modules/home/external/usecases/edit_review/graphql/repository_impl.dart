import 'package:either_dart/either.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import '../../../../domain/usecases/edit_review/params.dart';
import '../../../../domain/usecases/edit_review/exceptions.dart' as exceptions;
import '../../../../domain/usecases/edit_review/repository.dart';
import '../../../../domain/usecases/edit_review/result_typedef.dart';
import 'mapper.dart';

class RepositoryImpl implements Repository {
  final GraphQLClient client;
  final Mapper _mapper;

  RepositoryImpl(this.client, this._mapper);

  @override
  Result call(Params params) async {
    try {
      final QueryResult result = await client.mutate(
        MutationOptions(
          document: gql(_mutation),
          variables: {
            "id": params.review.id,
            "title": params.review.title,
            "body": params.review.body,
            "rating": params.review.stars.index + 1,
          },
        ),
      );

      if (result.hasException) {
        return const Left(exceptions.ExternalException(message: 'Unable to establish a connection'));
      }

      final resultData = result.data;

      try {
        if (resultData != null) {
          final formattedResult = _mapper.fromResultToEntity(resultData);
          return Right(formattedResult);
        }
      } catch (e) {
        return const Left(exceptions.UnknownException(message: 'Unable to edit a review'));
      }
      return const Left(exceptions.UnknownException(message: 'Unable to edit a review'));
    } catch (e) {
      return const Left(exceptions.UnknownException(message: 'Unexpected result: Edit review result'));
    }
  }

  final _mutation = r"""
          mutation EditReview($id: UUID!, $title: String!, $body: String!, $rating: Int!) {
            updateMovieReviewById(
              input: {movieReviewPatch: {rating: $rating, title: $title, body: $body}, id: $id}
            ) {
              clientMutationId
            }
          }
        """;
}
