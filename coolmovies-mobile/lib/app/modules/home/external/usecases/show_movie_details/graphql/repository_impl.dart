import 'package:either_dart/either.dart';
import 'package:graphql_flutter/graphql_flutter.dart' as graph;

import '../../../../domain/usecases/show_movie_details/exceptions.dart' as exceptions;
import '../../../../domain/usecases/show_movie_details/params.dart';
import '../../../../domain/usecases/show_movie_details/repository.dart';
import '../../../../domain/usecases/show_movie_details/result_typedef.dart';
import 'mapper.dart';

class RepositoryImpl implements Repository {
  final graph.GraphQLClient _client;
  final Mapper _mapper;

  RepositoryImpl(this._client, this._mapper);

  @override
  Result call(Params params) async {
    try {
      final result = await _client.query(
        graph.QueryOptions(
          //TODO(adbysantos) Look for a better solution involving cache. Using noCache temporarily.
          fetchPolicy: graph.FetchPolicy.noCache,
          document: graph.gql(_query),
          variables: {
            "id": params.id
          },
        ),
      );

      if (result.hasException) {
        return const Left(exceptions.ExternalException(message: 'Unable to establish a connection'));
      }

      final resultData = result.data;

      if (resultData != null) {
        final formattedResult = _mapper.fromResultToEntity(resultData);
        return Right(formattedResult);
      }

      return const Left(exceptions.UnknownException(message: "Unable to list movie's details"));
    } catch (e) {
      return const Left(exceptions.UnknownException(message: "Unexpected result: movie's details"));
    }
  }

  final _query = """
          query MovieById(\$id: UUID!) {
            movieById(id: \$id) {
              id
              imgUrl
              movieDirectorId
              releaseDate
              title
              userByUserCreatorId {
                name
              }
              movieReviewsByMovieId {
                totalCount
                nodes {
                  title
                  body
                  id
                  rating
                  userByUserReviewerId {
                    id
                    name
                  }
                }
              }
              movieDirectorByMovieDirectorId {
                name
              }
            }
          }
        """;
}
