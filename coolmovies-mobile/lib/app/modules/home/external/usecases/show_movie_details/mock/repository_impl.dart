import 'package:either_dart/either.dart';

import '../../../../domain/usecases/show_movie_details/params.dart';
import '../../../../domain/usecases/show_movie_details/repository.dart';
import '../../../../domain/usecases/show_movie_details/result_typedef.dart';
import 'mapper.dart';

class RepositoryImpl implements Repository {
  final Mapper _mapper;

  RepositoryImpl(this._mapper);

  @override
  Result call(Params params) async => Right(_mapper.fromResultToEntity({}));
}
