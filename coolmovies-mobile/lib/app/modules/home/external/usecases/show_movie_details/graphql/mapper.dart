import '../../../../domain/usecases/show_movie_details/entity.dart';
import '../../../../domain/usecases/show_movie_details/exceptions.dart';
import '../../../../domain/usecases/show_movie_details/sub_entities/movie_details.dart';
import '../../../../domain/usecases/show_movie_details/sub_entities/review.dart';
import '../../../../domain/usecases/show_movie_details/sub_entities/reviewer.dart';
import '../../../../domain/usecases/show_movie_details/sub_entities/stars.dart';

class Mapper {
  Entity fromResultToEntity(Map<String, dynamic> result) {
    if (!result.containsKey('movieById')) {
      throw const MapperException(message: 'Unexpected result format: movieById');
    }
    final movieById = result['movieById'];
    if (movieById is! Map) {
      throw const MapperException(message: 'Unexpected result type: movieById');
    }

    if (!movieById.containsKey('id')) {
      throw const MapperException(message: "Movie's id is missing");
    }

    final id = movieById['id'];

    if (id is! String) {
      throw const MapperException(message: "Unexpected type: Movie's id");
    }

    if (!movieById.containsKey('title')) {
      throw const MapperException(message: "Movie's title is missing");
    }

    final title = movieById['title'];

    if (title is! String) {
      throw const MapperException(message: "Unexpected type: Movie's title");
    }

    if (!movieById.containsKey('imgUrl')) {
      throw const MapperException(message: "Movie's imgUrl is missing");
    }

    final imgUrl = movieById['imgUrl'];

    if (imgUrl is! String) {
      throw const MapperException(message: "Unexpected type: Movie's imgUrl");
    }
    if (!movieById.containsKey('movieReviewsByMovieId')) {
      throw const MapperException(message: "Movie's movieReviewsByMovieId is missing");
    }

    final movieReviewsByMovieId = movieById['movieReviewsByMovieId'];

    if (movieReviewsByMovieId is! Map) {
      throw const MapperException(message: "Unexpected type: Movie's movieReviewsByMovieId");
    }

    if (!movieReviewsByMovieId.containsKey('nodes')) {
      throw const MapperException(message: "Review list is missing");
    }

    final nodes = movieReviewsByMovieId['nodes'];

    if (nodes is! List) {
      throw const MapperException(message: "Unexpected type: Review list");
    }

    final reviews = <Review>[];

    for (final review in nodes) {
      if (review is! Map) {
        throw const MapperException(message: 'Unexpected type: review node');
      }

      if (!review.containsKey('id')) {
        throw const MapperException(message: "Review's id is missing");
      }

      final id = review['id'];

      if (id is! String) {
        throw const MapperException(message: "Unexpected type: Review's id");
      }

      if (!review.containsKey('body')) {
        throw const MapperException(message: "Review's body is missing");
      }

      final body = review['body'];

      if (body is! String) {
        throw const MapperException(message: "Unexpected type: Review's body");
      }
      if (!review.containsKey('title')) {
        throw const MapperException(message: "Review's title is missing");
      }

      final title = review['title'];

      if (title is! String) {
        throw const MapperException(message: "Unexpected type: Review's title");
      }
      if (!review.containsKey('rating')) {
        throw const MapperException(message: "Review's rating is missing");
      }

      final rating = review['rating'];

      if (rating is! int) {
        throw const MapperException(message: "Unexpected type: Review's rating");
      }

      if (rating < 1 || rating > 5) {
        throw MapperException(message: "Review's rating value is unexpected: $rating");
      }

      final stars = Stars.values.elementAt(rating - 1);

      if (!review.containsKey('userByUserReviewerId')) {
        throw const MapperException(message: "The review's reviewer is missing");
      }

      final reviewerData = review['userByUserReviewerId'];

      if (reviewerData is! Map) {
        throw const MapperException(message: "Unexpected type: review's reviewer");
      }

      if (!reviewerData.containsKey('name')) {
        throw const MapperException(message: "The review's reviewer's name is missing");
      }

      final reviewerName = reviewerData['name'];

      if (reviewerName is! String) {
        throw const MapperException(message: "Unexpected type: review's reviewer's name");
      }

      if (!reviewerData.containsKey('id')) {
        throw const MapperException(message: "The review's reviewer's id is missing");
      }

      final reviewerId = reviewerData['id'];

      if (reviewerId is! String) {
        throw const MapperException(message: "Unexpected type: review's reviewer's id");
      }

      final reviewEntity = Review(id: id, title: title, body: body, stars: stars, reviewer: Reviewer(id: reviewerId, name: reviewerName));

      reviews.add(reviewEntity);
    }

    if (!movieById.containsKey('movieDirectorByMovieDirectorId')) {
      throw const MapperException(message: "The director of the movie is missing");
    }

    final movieDirector = movieById['movieDirectorByMovieDirectorId'];

    if (movieDirector is! Map) {
      throw const MapperException(message: "Unexpected type: movie director");
    }
    if (!movieDirector.containsKey('name')) {
      throw const MapperException(message: "The movie director's name is missing");
    }

    final directorName = movieDirector['name'];

    if (directorName is! String) {
      throw const MapperException(message: "Unexpected type: movie releaseDate");
    }
    if (!movieById.containsKey('releaseDate')) {
      throw const MapperException(message: "The movie releaseDate is missing");
    }

    final releaseDate = movieById['releaseDate'];

    if (releaseDate is! String) {
      throw const MapperException(message: "Unexpected type: movie releaseDate");
    }

    final movieDetails = MovieDetails(id: id, title: title, imgUrl: imgUrl, reviews: reviews, directorName: directorName, releaseDate: releaseDate);

    return Entity(movieDetails: movieDetails);
  }

  const Mapper();
}
