import '../../../../domain/usecases/show_movie_details/entity.dart';
import '../../../../domain/usecases/show_movie_details/sub_entities/movie_details.dart';
import '../../../../domain/usecases/show_movie_details/sub_entities/review.dart';
import '../../../../domain/usecases/show_movie_details/sub_entities/reviewer.dart';
import '../../../../domain/usecases/show_movie_details/sub_entities/stars.dart';

class Mapper {
  Entity fromResultToEntity(Map<String, dynamic> result) {
    const id = 'Movie id';
    const title = 'Movie title';
    const reviewId = 'Review id';
    const reviews = <Review>[
      Review(title: 'Review title', body: 'Review body', stars: Stars.five, reviewer: Reviewer(id: reviewId, name: 'Reviewer name'), id: '')
    ];

    const movieDetails = MovieDetails(id: id, title: title, imgUrl: 'https://api.lorem.space/image/movie?w=40&h=80&hash=g726ljt4', reviews: reviews, directorName: '', releaseDate: '2022-06-27');

    return const Entity(movieDetails: movieDetails);
  }

  const Mapper();
}
