import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../app_module.dart';
import 'controller.dart';

class Page extends StatefulWidget {
  const Page({super.key});

  @override
  State<Page> createState() => _PageState();
}

class _PageState extends State<Page> {
  final controller = Modular.get<Controller>();

  bool get isLogged => controller.isLogged;

  @override
  void initState() {
    super.initState();
    Future.wait([
      controller.initializeApp(),
      Modular.isModuleReady<AppModule>(),
    ]).then((value) {
      if (isLogged) {
        Modular.to.pushReplacementNamed('home/');
      } else {
        Modular.to.pushReplacementNamed('auth/');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Splash'),
      ),
      body: const Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircularProgressIndicator(),
          ],
        ),
      ),
    );
  }
}
