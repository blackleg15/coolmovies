import '../../core/services/storage/interface.dart';
import '../../core/session/session_store.dart';

class Controller {
  final Storage<String> _storage;
  final SessionStore _sessionStore;

  Controller(this._storage, this._sessionStore);

  Future initializeApp() => _storage.init().then((value) => _sessionStore.init());

  bool get isLogged => _sessionStore.isLogged;
}
