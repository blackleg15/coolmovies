import '../../../../../core/entities/logged_user.dart';

class Entity {
  final LoggedUser loggedUser;

  const Entity({required this.loggedUser});
}
