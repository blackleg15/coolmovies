class Entity {
  final List<User> users;

  const Entity({required this.users});
}

class User {
  final String id;
  final String name;
  const User({
    required this.id,
    required this.name,
  });
}
