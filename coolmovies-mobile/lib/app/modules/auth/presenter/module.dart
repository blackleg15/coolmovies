import 'package:flutter_modular/flutter_modular.dart';

import '../../auth/presenter/pages/main/controller.dart';
import 'pages/main/blocs/usecases/existent_users/imports.dart' as existent_users;
import 'pages/main/blocs/usecases/create_user/imports.dart' as create_user;
import 'pages/main/page.dart';

class AuthModule extends Module {
  @override
  List<Bind<Object>> get binds => [
        Bind.lazySingleton<Controller>((i) => Controller(i(), i(), i()), onDispose: (bloc) => bloc.close()),
        ...existent_users.usecaseBinds,
        ...create_user.usecaseBinds,
      ];

  @override
  List<ModularRoute> get routes => [
        ChildRoute('/', child: (context, args) => const Page())
      ];
}
