import 'package:flutter_modular/flutter_modular.dart';

import '../../../../../../domain/usecases/create_user/usecase_impl.dart';
import '../../../../../../external/usecases/create_user/graphql/repository_impl.dart';
import '../../../../../../external/usecases/create_user/graphql/mapper.dart';
import 'bloc.dart';

final usecaseBinds = <Bind>[
  Bind.singleton<UsecaseBloc>((i) => UsecaseBloc(i()), onDispose: (bloc) => bloc.close()),
  Bind.singleton((i) => UsecaseImpl(i())),
  Bind.singleton((i) => RepositoryImpl(i(), i())),
  Bind.singleton((i) => const Mapper()),
  //Bind.singleton((i) => RepositoryImpl(i())),
  //Bind.singleton((i) => const Mapper()),
];
