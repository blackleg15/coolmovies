import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../../../core/entities/logged_user.dart';
import '../../../../../core/session/session_store.dart';
import '../../../domain/usecases/existent_users/params.dart' as existent_users;
import '../../../domain/usecases/create_user/params.dart';
import 'blocs/controller_events.dart';
import 'blocs/controller_states.dart';
import 'blocs/usecases/existent_users/bloc.dart' as existent_users;
import 'blocs/usecases/create_user/bloc.dart' as create_user;

class Controller extends Bloc<ControllerEvent, ControllerState> {
  final SessionStore _sessionStore;
  final existent_users.UsecaseBloc existentUsersBloc;
  final create_user.UsecaseBloc createUserBloc;
  Completer<create_user.BlocState>? _createNewUserCompleter;

  Controller(this._sessionStore, this.existentUsersBloc, this.createUserBloc) : super(const InitialLoading()) {
    existentUsersBloc.add(const existent_users.GetExistentUsers(existent_users.Params()));
    createUserBloc.stream.listen((event) {
      if (event is create_user.ExceptionState || event is create_user.SuccessState) {
        _createNewUserCompleter!.complete(event);
      }
    });
    on<SetAlreadyExistentUser>((event, emit) async {
      emit(const Loading());
      await _useAlreadyExistentUser(event.user).then((value) {
        emit(const Idle());
        Modular.to.pushReplacementNamed('../home/');
      });
    });
    on<CreateNewUser>((event, emit) async {
      emit(const Loading());
      _createNewUserCompleter = Completer<create_user.BlocState>();
      createUserBloc.add(create_user.CreateUser(Params(username: event.username)));
      final newState = await _createNewUserCompleter!.future;
      if (newState is create_user.ExceptionState) {
        emit(const Idle());
      } else if (newState is create_user.SuccessState) {
        _setAppUser(newState.user);
        emit(const Idle());
        Modular.to.pushReplacementNamed('../home/');
      }
    });
  }

  Future<void> _setAppUser(LoggedUser loggedUser) {
    return _sessionStore.storeNewSession(loggedUser);
  }

  Future<void> _useAlreadyExistentUser(LoggedUser loggedUser) {
    return _setAppUser(loggedUser);
  }
}
