part of 'bloc.dart';

class BlocState {
  final List<User> users;

  const BlocState({required this.users});
}

class InitialState extends BlocState {
  const InitialState() : super(users: const []);
}

class LoadingState extends BlocState {
  const LoadingState(List<User> users) : super(users: users);
}

class SuccessState extends BlocState {
  const SuccessState(List<User> users) : super(users: users);
}

class ExceptionState extends BlocState {
  final Exception exception;

  const ExceptionState(List<User> users, this.exception) : super(users: users);
}
