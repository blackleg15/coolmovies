abstract class ControllerState {
  final bool hasTypedAnyKey;

  const ControllerState({required this.hasTypedAnyKey});
}

class InitialLoading extends ControllerState {
  const InitialLoading() : super(hasTypedAnyKey: false);
}

class Initial extends ControllerState {
  const Initial() : super(hasTypedAnyKey: false);
}

class Idle extends ControllerState {
  const Idle() : super(hasTypedAnyKey: true);
}

class Loading extends ControllerState {
  const Loading() : super(hasTypedAnyKey: true);
}
