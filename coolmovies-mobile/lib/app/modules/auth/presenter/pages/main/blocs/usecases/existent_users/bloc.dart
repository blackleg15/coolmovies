import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../../domain/usecases/existent_users/entity.dart';
import '../../../../../../domain/usecases/existent_users/params.dart';
import '../../../../../../domain/usecases/existent_users/usecase.dart';
import '../../../../../../domain/usecases/existent_users/exceptions.dart';

part 'events.dart';
part 'states.dart';

class UsecaseBloc extends Bloc<BlocEvent, BlocState> {
  final Usecase usecase;

  UsecaseBloc(this.usecase) : super(const InitialState()) {
    on<GetExistentUsers>((event, emit) async {
      emit(LoadingState(state.users));
      final result = await usecase(event.params);
      final newState = result.fold(
        (exception) => ExceptionState(state.users, exception),
        (entity) => SuccessState(entity.users),
      );
      emit(newState);
    });
  }
}
