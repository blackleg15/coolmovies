import '../../../../../../core/entities/logged_user.dart';

abstract class ControllerEvent {}

class SetAlreadyExistentUser extends ControllerEvent {
  final LoggedUser user;

  SetAlreadyExistentUser(this.user);
}

class CreateNewUser extends ControllerEvent {
  final String username;

  CreateNewUser(this.username);
}
