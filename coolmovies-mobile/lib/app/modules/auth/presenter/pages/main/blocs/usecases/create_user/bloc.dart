import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../../../../core/entities/logged_user.dart';
import '../../../../../../domain/usecases/create_user/params.dart';
import '../../../../../../domain/usecases/create_user/usecase.dart';
import '../../../../../../domain/usecases/create_user/exceptions.dart';

part 'events.dart';
part 'states.dart';

class UsecaseBloc extends Bloc<BlocEvent, BlocState> {
  final Usecase getMovies;

  UsecaseBloc(this.getMovies) : super(const InitialState()) {
    on<CreateUser>((event, emit) async {
      emit(const LoadingState());
      final result = await getMovies(event.params);
      final newState = result.fold(
        (exception) => ExceptionState(exception),
        (entity) => SuccessState(entity.loggedUser),
      );
      emit(newState);
    });
    on<ValidateUsernameField>((event, emit) async {
      final username = event.username;
      String? usernameError;
      if (username.isEmpty) {
        usernameError = 'Type an username';
      } else if (event.isThisUsernameAlreadyInUse(username)) {
        usernameError = 'That username is already in use';
      }
      emit(TypingState(usernameError));
    });
  }
}
