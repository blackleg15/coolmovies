part of 'bloc.dart';

abstract class BlocEvent {
  const BlocEvent();
}

class ValidateUsernameField extends BlocEvent {
  final String username;
  final bool Function(String username) isThisUsernameAlreadyInUse;

  const ValidateUsernameField(this.username, this.isThisUsernameAlreadyInUse);
}

class CreateUser extends BlocEvent {
  final Params params;

  const CreateUser(this.params);
}
