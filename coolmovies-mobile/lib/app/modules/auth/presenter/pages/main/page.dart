import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../../../core/entities/logged_user.dart';
import 'blocs/controller_events.dart';
import 'blocs/usecases/existent_users/bloc.dart';
import 'controller.dart';
import 'blocs/usecases/create_user/bloc.dart' as create_user;

class Page extends StatefulWidget {
  const Page({super.key});

  @override
  State<Page> createState() => _PageState();
}

class _PageState extends State<Page> {
  final controller = Modular.get<Controller>();
  String newUsername = '';

  void validateUsername() {
    final cleanUsername = newUsername.trim().toLowerCase();
    controller.createUserBloc.add(create_user.ValidateUsernameField(cleanUsername, isThisUsernameAlreadyInUse));
  }

  bool isThisUsernameAlreadyInUse(String username) {
    final existentUsers = controller.existentUsersBloc.state.users;
    return existentUsers.any((existentUser) => existentUser.name.toLowerCase() == username);
  }

  @override
  Widget build(BuildContext context) {
    final bottomInsets = MediaQuery.of(context).viewInsets.bottom;
    final isKeyboardOpened = bottomInsets > 0;
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            const Expanded(
              flex: 3,
              child: Center(
                child: Text(
                  'Movieflix',
                  style: TextStyle(fontSize: 45, fontWeight: FontWeight.bold),
                ),
              ),
            ),
            AnimatedPadding(
              padding: EdgeInsets.only(top: isKeyboardOpened ? 0 : 20),
              duration: const Duration(milliseconds: 50),
            ),
            BlocBuilder<create_user.UsecaseBloc, create_user.BlocState>(
              bloc: controller.createUserBloc,
              builder: (context, state) => Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text('Create your own user:'),
                  AnimatedPadding(
                    padding: EdgeInsets.only(top: isKeyboardOpened ? 0 : 5),
                    duration: const Duration(milliseconds: 50),
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 30),
                    child: TextField(
                      onChanged: (value) {
                        newUsername = value;
                        validateUsername();
                      },
                      decoration: InputDecoration(
                        hintText: 'Type your username',
                        hintStyle: TextStyle(color: Colors.white.withOpacity(0.2), fontSize: 13),
                        floatingLabelStyle: const TextStyle(
                          color: Colors.white,
                        ),
                        labelStyle: const TextStyle(
                          color: Colors.white,
                          fontSize: 14,
                        ),
                        errorText: state.usernameFieldError,
                      ),
                      style: const TextStyle(color: Colors.white),
                    ),
                  ),
                  AnimatedPadding(
                    padding: EdgeInsets.only(top: isKeyboardOpened ? 0 : 5),
                    duration: const Duration(milliseconds: 50),
                  ),
                  ElevatedButton(
                    onPressed: !state.hasTypedAnyKey || state.usernameFieldError != null
                        ? null
                        : () {
                            final name = newUsername.trim();
                            controller.add(CreateNewUser(name));
                          },
                    style: ElevatedButton.styleFrom(foregroundColor: state.hasTypedAnyKey && state.usernameFieldError == null ? Colors.blue : Theme.of(context).disabledColor),
                    child: Text(
                      'Create user',
                      style: TextStyle(color: state.hasTypedAnyKey && state.usernameFieldError == null ? const Color(0xFF212121) : Colors.white.withOpacity(0.2)),
                    ),
                  ),
                ],
              ),
            ),
            AnimatedPadding(
              padding: EdgeInsets.only(top: isKeyboardOpened ? 0 : 20),
              duration: const Duration(milliseconds: 50),
            ),
            const Center(
              child: Text(
                'OR',
                style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
              ),
            ),
            AnimatedPadding(
              padding: EdgeInsets.only(top: isKeyboardOpened ? 0 : 20),
              duration: const Duration(milliseconds: 50),
            ),
            Expanded(
              flex: 4,
              child: Column(
                children: [
                  const Text('Choose an existent user'),
                  AnimatedPadding(
                    padding: EdgeInsets.only(top: isKeyboardOpened ? 0 : 10),
                    duration: const Duration(milliseconds: 50),
                  ),
                  Expanded(
                    child: BlocBuilder(
                      bloc: controller.existentUsersBloc,
                      builder: (context, state) {
                        if (state is LoadingState) {
                          return const Center(
                            child: CircularProgressIndicator.adaptive(),
                          );
                        }
                        if (state is ExceptionState) {
                          return Center(
                            child: Text(state.exception.message),
                          );
                        }
                        if (state is SuccessState) {
                          final existentUsers = state.users;
                          return ListView.builder(
                            padding: const EdgeInsets.symmetric(horizontal: 24),
                            itemCount: existentUsers.length,
                            itemBuilder: (context, index) {
                              final user = existentUsers[index];
                              return ListTile(
                                title: Text(
                                  '#${index + 1} | ${user.name}',
                                  style: const TextStyle(fontWeight: FontWeight.bold),
                                ),
                                onTap: () {
                                  controller.add(SetAlreadyExistentUser(LoggedUser(id: user.id, name: user.name)));
                                },
                              );
                            },
                          );
                        }
                        return const SizedBox();
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
