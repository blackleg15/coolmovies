part of 'bloc.dart';

abstract class BlocEvent {
  const BlocEvent();
}

class GetExistentUsers extends BlocEvent {
  final Params params;

  const GetExistentUsers(this.params);
}
