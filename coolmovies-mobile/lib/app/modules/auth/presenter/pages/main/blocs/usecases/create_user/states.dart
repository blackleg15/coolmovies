part of 'bloc.dart';

abstract class BlocState {
  final bool hasTypedAnyKey;
  final String? usernameFieldError;

  const BlocState({this.hasTypedAnyKey = false, this.usernameFieldError});
}

class InitialState extends BlocState {
  const InitialState();
}

class TypingState extends BlocState {
  const TypingState(String? usernameFieldError) : super(hasTypedAnyKey: true, usernameFieldError: usernameFieldError);
}

class LoadingState extends BlocState {
  const LoadingState() : super(hasTypedAnyKey: true);
}

class SuccessState extends BlocState {
  final LoggedUser user;

  const SuccessState(this.user) : super(hasTypedAnyKey: true);
}

class ExceptionState extends BlocState {
  final Exception exception;

  const ExceptionState(this.exception, {super.usernameFieldError}) : super(hasTypedAnyKey: true);
}
