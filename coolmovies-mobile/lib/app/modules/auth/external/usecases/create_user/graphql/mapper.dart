import 'package:either_dart/either.dart';

import '../../../../../../core/entities/logged_user.dart';
import '../../../../domain/usecases/create_user/entity.dart';
import '../../../../domain/usecases/create_user/exceptions.dart';

class Mapper {
  Either<Exception, Entity> fromResultToEntity(Map<String, dynamic> result) {
    try {
      if (!result.containsKey('createUser')) {
        throw "'Create user' doesn't exist";
      }
      final createUser = result['createUser'];

      if (createUser is! Map) {
        throw "'Create user' field is not expected";
      }
      if (!createUser.containsKey('user')) {
        throw "'User' field does not exist'";
      }
      final user = createUser['user'];

      if (user is! Map) {
        throw "'User' field is not expected";
      }

      if (!user.containsKey('id')) {
        throw "User's 'id' field does not exist'";
      }
      final id = user['id'];

      if (id is! String) {
        throw "User's 'id' field is not expected";
      }
      if (!user.containsKey('name')) {
        throw "User's 'name' field does not exist'";
      }
      final name = user['name'];

      if (name is! String) {
        throw "User's 'name' field is not expected";
      }

      return Right(Entity(loggedUser: LoggedUser(id: id, name: name)));
    } catch (e) {
      return Left(MapperException(message: e.toString()));
    }
  }

  const Mapper();
}
