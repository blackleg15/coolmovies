import 'package:either_dart/either.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import '../../../../domain/usecases/create_user/exceptions.dart' as exceptions;
import '../../../../domain/usecases/create_user/params.dart';
import '../../../../domain/usecases/create_user/repository.dart';
import '../../../../domain/usecases/create_user/result_typedef.dart';
import 'mapper.dart';

class RepositoryImpl implements Repository {
  final GraphQLClient client;
  final Mapper _mapper;

  RepositoryImpl(this._mapper, this.client);

  @override
  Result call(Params params) async {
    try {
      final QueryResult result = await client.mutate(
        MutationOptions(
          document: gql(_mutation),
          variables: {
            "name": params.username,
          },
        ),
      );

      if (result.hasException) {
        return const Left(exceptions.ExternalException(message: 'Unable to establish a connection'));
      }

      final resultData = result.data;

      if (resultData != null) {
        final formattedResult = _mapper.fromResultToEntity(resultData);
        return formattedResult.fold((left) => Left(left), (right) => Right(right));
      }
      return const Left(exceptions.UnknownException(message: 'Unable to create a user'));
    } catch (e) {
      return const Left(exceptions.UnknownException(message: 'Unexpected result: Create user result result'));
    }
  }

  final _mutation = r'''
            mutation CreateUser($name: String!) {
              createUser(input: {user: {name: $name}}) {
                user {
                  id
                  name
                }
              }
            }
        ''';
}
