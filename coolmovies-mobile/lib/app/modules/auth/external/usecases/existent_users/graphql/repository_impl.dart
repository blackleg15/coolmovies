import 'package:either_dart/either.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import '../../../../domain/usecases/existent_users/exceptions.dart' as exceptions;
import '../../../../domain/usecases/existent_users/params.dart';
import '../../../../domain/usecases/existent_users/repository.dart';
import '../../../../domain/usecases/existent_users/result_typedef.dart';
import 'mapper.dart';

class RepositoryImpl implements Repository {
  final GraphQLClient client;
  final Mapper _mapper;

  RepositoryImpl(this._mapper, this.client);

  @override
  Result call(Params params) async {
    try {
      final QueryResult result = await client.query(
        QueryOptions(
          fetchPolicy: FetchPolicy.noCache,
          document: gql(_query),
        ),
      );

      if (result.hasException) {
        return const Left(exceptions.ExternalException(message: 'Unable to establish a connection'));
      }

      final resultData = result.data;

      if (resultData != null) {
        final formattedResult = _mapper.fromResultToEntity(resultData);
        return formattedResult.fold((left) => Left(left), (right) => Right(right));
      }
      return const Left(exceptions.UnknownException(message: 'Unable to get existent users'));
    } catch (e) {
      return const Left(exceptions.UnknownException(message: 'Unexpected result: Existent users'));
    }
  }

  final _query = r'''
            query AllUsers {
              allUsers {
                nodes {
                  id
                  name
                }
              }
            }
        ''';
}
