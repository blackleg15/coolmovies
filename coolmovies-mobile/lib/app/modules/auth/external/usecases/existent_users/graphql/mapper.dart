import 'package:either_dart/either.dart';

import '../../../../domain/usecases/existent_users/entity.dart';
import '../../../../domain/usecases/existent_users/exceptions.dart';

class Mapper {
  Either<Exception, Entity> fromResultToEntity(Map<String, dynamic> result) {
    try {
      if (!result.containsKey('allUsers')) {
        throw "All users doesn't exist";
      }

      final allUsers = result['allUsers'];

      if (!allUsers.containsKey('nodes')) {
        throw "User list doesn't exist";
      }
      final rawUsers = allUsers['nodes'];

      if (rawUsers is! List) {
        throw "User list's type is unexpected";
      }

      if (rawUsers.isEmpty) {
        throw "User list is empty";
      }

      final users = <User>[];

      for (final user in rawUsers) {
        final id = user['id'];

        if (id is! String) {
          throw "User's 'id' field is not expected";
        }
        if (!user.containsKey('name')) {
          throw "User's 'name' field does not exist'";
        }
        final name = user['name'];

        if (name is! String) {
          throw "User's 'name' field is not expected";
        }

        users.add(User(id: id, name: name));
      }

      return Right(Entity(users: users));
    } catch (e) {
      return Left(MapperException(message: e.toString()));
    }
  }

  const Mapper();
}
