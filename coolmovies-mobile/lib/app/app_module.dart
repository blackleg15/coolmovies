import 'package:graphql_flutter/graphql_flutter.dart';

import 'core/services/storage/hive/hive_storage_impl.dart';
import 'core/services/storage/storage_type.dart';
import 'core/session/session_store.dart';
import 'modules/auth/presenter/module.dart';
import 'modules/home/presenter/module.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'modules/splash/page.dart' as splash;
import 'modules/splash/controller.dart';

class AppModule extends Module {
  @override
  final List<Bind<Object>> binds = [
    AsyncBind<GraphQLClient>((i) async {
      await initHiveForFlutter();
      const baseUri = 'http://192.168.0.101:5001/graphql';
      final HttpLink httpLink = HttpLink(baseUri);
      final AuthLink authLink = AuthLink(getToken: () async => 'Bearer <YOUR_PERSONAL_ACCESS_TOKEN>');
      final Link link = authLink.concat(httpLink);

      return GraphQLClient(
        link: link,
        cache: GraphQLCache(store: HiveStore()),
      );
    }),
    Bind.singleton((i) => HiveBoxStorage<String>(StorageType.user)),
    Bind.singleton((i) => SessionStore(i())),
    Bind.singleton((i) => Controller(i(), i())),
  ];

  @override
  final List<ModularRoute> routes = [
    ChildRoute('/', child: (context, args) => const splash.Page()),
    ModuleRoute('/home', module: HomeModule()),
    ModuleRoute('/auth', module: AuthModule()),
    //ChildRoute('/', child: (context, args) => const DesignSystemPage()),
  ];
}
