import 'dart:convert';

class LoggedUser {
  final String id;
  final String name;
  const LoggedUser({
    required this.id,
    required this.name,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
    };
  }

  factory LoggedUser.fromMap(Map<String, dynamic> map) {
    return LoggedUser(
      id: map['id'] ?? '',
      name: map['name'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory LoggedUser.fromJson(String source) => LoggedUser.fromMap(json.decode(source));
}
