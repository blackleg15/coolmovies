import 'dart:developer';

import '../entities/logged_user.dart';
import '../services/storage/interface.dart';

class SessionStore {
  final _loggedUserStorageKey = 'LoggedUser';
  final Storage<String> _storage;

  LoggedUser? loggedUser;
  String? exception;

  SessionStore(this._storage);

  Future<void> init() async {
    final result = await _checkStorageForStoredSessions();
    if (!_isThereAnyStoredSession(result)) {
      return log('Nenhum usuário armazenado');
    } else {
      log('Iniciando Session armazenada');
      final storedUser = _fromStorageFormatToLoggedUser(result!);
      _updateStoreWithANewLoggedUser(storedUser);
    }
  }

  Future<String?> _checkStorageForStoredSessions() async {
    return await _storage.get(_loggedUserStorageKey);
  }

  bool _isThereAnyStoredSession(String? result) {
    return !_isNullOrEmpty(result);
  }

  bool _isNullOrEmpty(String? value) {
    return value == null || value.isEmpty;
  }

  LoggedUser _fromStorageFormatToLoggedUser(String encodedJson) {
    return LoggedUser.fromJson(encodedJson);
  }

  void _updateStoreWithANewLoggedUser(LoggedUser user) {
    loggedUser = user;
  }

  Future<void> storeNewSession(LoggedUser user) async {
    await _storage.put(_loggedUserStorageKey, user.toJson());
    _updateStoreWithANewLoggedUser(user);
  }

  Future<void> logout() async {
    if (isLogged) {
      await _clearSessionFromStorage();
      _resetStore();
      log('Sessão finalizada');
    }
  }

  bool get isLogged => loggedUser != null;

  Future<void> _clearSessionFromStorage() {
    return _storage.put(_loggedUserStorageKey, '');
  }

  void _resetStore() {
    loggedUser = null;
  }
}
